# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: https://doc.scrapy.org/en/latest/topics/item-pipeline.html
import pymongo
import datetime
from scrapy import log

class EverytimePipeline(object):
    def process_item(self, item, spider):
        return item


# https://doc.scrapy.org/en/latest/topics/item-pipeline.html#write-items-to-mongodb
# http://excelsior-cjh.tistory.com/86?category=952027
class EverytimeMongoPipeline(object):

    def __init__(self, mongo_uri, mongo_db):
        self.mongo_uri = mongo_uri
        self.mongo_db = mongo_db

    @classmethod
    def from_crawler(cls, crawler):
        return cls(
            mongo_uri=crawler.settings.get('MONGO_URI'),
            mongo_db=crawler.settings.get('MONGO_DATABASE', 'items')
        )

    def open_spider(self, spider):
        self.client = pymongo.MongoClient(self.mongo_uri)
        self.db = self.client[self.mongo_db]

    def close_spider(self, spider):
        self.client.close()

    def process_item(self, item, spider):
        collection_name = item['moim']
        collection = self.db[collection_name]
        if collection.find_one({"article_id" : item["article_id"]}, max_time_ms=500) is not None:
            spider.logger.info(f"Drop duplicated record|{item['moim']} | {item['article_id']}")
            return
        self.db[collection_name].insert_one(dict(item))
        # self.db[self.collection_name].insert_one(dict(item))
        log.msg(f"Insert {item['article_id']}", level=log.DEBUG, spider=spider)
        return item

#TODO InstagramMongoPipeline
class InstagramMongoPipeline(object):

    def __init__(self, mongo_uri, mongo_db):
        self.mongo_uri = mongo_uri
        self.mongo_db = mongo_db
        kor_time_now = datetime.datetime.utcnow() + datetime.timedelta(hours=9)
        self.collection_name = kor_time_now.strftime("%Y%m%d")

    @classmethod
    def from_crawler(cls, crawler):
        return cls(
            mongo_uri=crawler.settings.get('MONGO_URI'),
            mongo_db=crawler.settings.get('MONGO_DATABASE', 'items')
        )

    def open_spider(self, spider):
        self.client = pymongo.MongoClient(self.mongo_uri)
        self.db = self.client[self.mongo_db]

    def close_spider(self, spider):
        self.client.close()

    def process_item(self, item, spider):
        collection_name = self.collection_name
        collection = self.db[collection_name]
        collection.insert_one(dict(item))
        # self.db[self.collection_name].insert_one(dict(item))
        log.msg(f"Insert {item['shortcode']}", level=log.DEBUG, spider=spider)
        return item