import scrapy
import datetime
import os
from everytime import items

class EverytimeSpider(scrapy.Spider):
    name="everytime"
    start_urls = ['https://everytime.kr/login']
    
    custom_settings = {
        "MONGO_DATABASE" : 'Yonsei_everytime',
        'ITEM_PIPELINES': {
            'everytime.pipelines.EverytimeMongoPipeline': 300,
        },
        'LOG_FILE' : 'everytime/logs/everytime.log',
        "EXTENSIONS" : {
            'scrapy_kafka_export.KafkaItemExporterExtension' : 1,
        },
        "KAFKA_EXPORT_ENABLED" : True,
        "KAFKA_BROKERS" : ['kafka:9092'],
        "KAFKA_TOPIC" : 'everytime',
    }

    
    def __init__(self, end=None, *args, **kwargs):
        super(EverytimeSpider, self).__init__(*args, **kwargs)
        if end==None:
            kor_time_now = datetime.datetime.utcnow() + datetime.timedelta(hours=9)
            self.end = kor_time_now - datetime.timedelta(minutes=1)
        else:
            self.end = datetime.datetime.strptime(end, "%Y%m%d%H%M%S")

    # https://doc.scrapy.org/en/latest/topics/request-response.html#using-formrequest-from-response-to-simulate-a-user-login
    def parse(self, response):
        from .everytime_config import get_login_info
        # Post id and password for everytime. id, password setting is in everytime_config.py
        return scrapy.FormRequest.from_response(
            response,
            formdata=get_login_info(),
            callback=self.after_login,
        )

    def after_login(self, response):
        # check Login fail
        if response.url == 'https://everytime.kr/login':
            self.logger.error("Login failed")
            return
        else:
            self.logger.info("Login success")
            # Parsing board url and call parse_post_url function
            for board_url in response.xpath('//*[@id="submenu"]/div').xpath('.//a/@href')[:-1]:
                # Example
                # post : id=370438&limit_num=20&start_num=0&moiminfo=true
                # board_url = /270438
                moim_id = board_url.extract()[1:]
                self.logger.info(f"Call paese_post_url |{moim_id}")
                yield scrapy.FormRequest(url="https://everytime.kr/find/board/article/list",
                    formdata={"id":moim_id,
                        "limit_num":"20",
                        "start_num":"0",
                        "moiminfo":"true"},
                    callback=self.parse_post_url,
                    meta={'start_num':"0"})
    
    def parse_post_url(self, response):
        check_new_page=True      
        # Get moim information
        moim_id = response.xpath("//moim/@id").extract_first()
        moim = response.xpath("//moim/@name").extract_first()
        self.logger.info(f"Called parse_post_url |{moim} | {moim_id} | {response.request.body}")
        try:
            post_urls = response.xpath("//article")
            # Parsing post url and call parse_post function
            for post_url in post_urls:
                # Get when the post is posted
                created_at = datetime.datetime.strptime(
                    post_url.xpath("@created_at").extract_first(),
                    "%Y-%m-%d %H:%M:%S")
                # If post is order than we want, stop parsing the post url
                if self.end > created_at:
                    check_new_page = False
                    self.logger.info(f"Stop parse_post_url | {moim} | {moim_id} |end time {self.end} | stop at {created_at}")
                    break
                # Using post url, call post_parse function
                post_id = post_url.xpath('@id').extract_first()
                self.logger.info(f"Call parse_post | {post_id} | {created_at}")
                yield scrapy.FormRequest(
                    url="https://everytime.kr/find/board/comment/list",
                    formdata = {"id" : post_id,
                        "limit_num" : "-1",
                        "moiminfo" : "false"},
                    callback=self.parse_post,
                    meta={"moim":moim, "retry": 0})
            
            # Until the end of page, if every posts are created earlier than end date, move to next page
            if check_new_page and post_urls != []:
                next_start_num = str(int(response.meta['start_num'])+20)
                self.logger.info(f"Move to next page of {moim_id} start from {next_start_num}")
                yield scrapy.FormRequest(url="https://everytime.kr/find/board/article/list",
                        formdata={"id":moim_id,
                            "limit_num":"20",
                            "start_num":next_start_num,
                            "moiminfo":"true"},
                        callback=self.parse_post_url,
                        meta={'start_num':next_start_num})
        except Exception as e:
            self.logger.error(f"{e}\nrequest body: {response.request.body}\nesponse body : {response.body}")
            self.logger.error(post_urls)

    def parse_post(self, response):
        post_item = items.EverytimePostItem()
        article = response.xpath("//article")
        self.logger.info(f"Start parse_post | {article.xpath('@id').extract_first()} | {response.status}")

        post_item['moim'] = response.meta['moim']
        post_item['article_id'] = article.xpath('@id').extract_first()
        post_item['title'] = article.xpath('@title').extract_first()
        post_item['text'] = article.xpath('@text').extract_first()
        post_item['created_at'] = article.xpath('@created_at').extract_first()
        post_item['posvote'] = article.xpath('@posvote').extract_first()
        post_item['comment_num'] = article.xpath('@comment').extract_first()
        post_item['comment'] = list()


        for comment in response.xpath('//comment'):
            comment_item = items.EverytimeCommentItem()
            comment_item['comment_id'] = comment.xpath('@id').extract_first()
            comment_item['text'] = comment.xpath('@text').extract_first()
            comment_item['created_at'] = comment.xpath('@created_at').extract_first()
            post_item['comment'].append(dict(comment_item))

        if post_item['created_at'] is None:
            self.logger.warn(f"Response is not perpect | {response.body}")
            if response.meta['retry'] < 3:
                self.logger.info(f"Response retry | {post_item['article_id']}")
                yield scrapy.FormRequest(
                    url="https://everytime.kr/find/board/comment/list",
                    formdata = {"id" : post_item['article_id'],
                        "limit_num" : "-1",
                        "moiminfo" : "true"},
                    callback=self.parse_post,
                    meta={"moim":post_item['moim'], "retry": response.meta['retry']+1},
                    dont_filter=True,)
            else:
                self.logger.error(f"Response Max Retry {response.meta['retry']} | {article}")
                yield post_item
        else:
            if response.meta['retry'] > 0:
                self.logger.info(f"Response Retry Success {post_item['article_id']}")
            yield post_item


            



# parse_post_url response example
# <?xml version="1.0" encoding="UTF-8"?>
# <response>
#   <moim id="255674" name="비밀게시판" info="" picture="" type="1" layout="11" priv_anonym="0" priv_comment_anonym="0" is_primary="1" is_schoolcommunity="1" is_secret="1" is_manageable="0" is_searchable="1" is_member="0" join_to_write="0" join_to_comment="0" auth_to_write="0" auth_to_comment="0" placeholder="여기를 눌러서 글을 작성할 수 있습니다. &lt;br />아래 내용을 모두 지켜서 깨끗한 에브리타임을 만들어주세요 :) &lt;br />&lt;br />[정보통신망법에 의한 홍보 게시물 작성 금지] &lt;br />1. 쇼핑몰, 오픈마켓, 소셜커머스 등 홍보 &lt;br />2. 광고가 있는 카페, 블로그, 커뮤니티, 앱의 추천, 홍보, 방문 유도 등 &lt;br />3. 토익, 한자 등 어학원 &lt;br />4. 연극, 영화 티켓 할인 서비스 &lt;br />5. 안과, 치과, 피부과 등 의료업체 &lt;br />&lt;br />[커뮤니티 이용규칙에 어긋나는 행위 금지] &lt;br />1. 비밀게시판 등 인증 후 사용할 수 있는 게시판의 스크린샷, 게시물 내용 유출 &lt;br />2. 욕설, 비하, 음란물, 개인정보가 포함된 게시물 게시 &lt;br />3. 특정인이나 단체/지역을 비방하는 행위 &lt;br />4. 기타 현행법에 어긋나는 행위 &lt;br />&lt;br />자세한 내용은 홈화면 상단 [내 정보]의 [커뮤니티 이용규칙]을 참고하시기 바랍니다." is_quick="1" is_accessible="1" is_readable="1" is_writable="1" is_commentable="1"/>
#   <hashtags/>
#   <article id="56940523" is_mine="0" title="" text="돕바 공구&lt;br />17일까지 문의 받습니다&lt;br />독수리 빼고도 제작 가능해요!&lt;br />업체는 버즈 팩토리고 가격은 6만2,000원&lt;br />관심 있으신 분들은 카카오톡 오픈채팅 들어와 주세요~!&lt;br />&lt;a href=&quot;https://open.kakao.com/o/g6WJxs4&quot; target=&quot;_blank&quot;>https://open.kakao.com/o/g6WJxs4&lt;/a>" created_at="2018-11-15 11:14:24" posvote="0" comment="0" comment_anonym="0" scrap_count="0" user_type="" user_id="0" user_nickname="익명" user_picture="https://cf-fpi.everytime.kr/0.png"/>
#   <article id="56940342" is_mine="0" title="" text="공디 지을때 경영관을 능가하는 최고의 똥스팟이 될거라고 생각했는데&lt;br />안락함이 없다&lt;br />인테리어가 추워서 얼른 응가하고 나오고 싶어" created_at="2018-11-15 11:11:20" posvote="0" comment="5" comment_anonym="0" scrap_count="0" user_type="" user_id="0" user_nickname="익명" user_picture="https://cf-fpi.everytime.kr/0.png"/>
#   <article id="56940203" is_mine="0" title="" text="며칠전에 턱까지 오는 단발로 머리짧랐는데 마음에 든다 &lt;br />4년만에 단발하네" created_at="2018-11-15 11:08:51" posvote="1" comment="1" comment_anonym="0" scrap_count="0" user_type="" user_id="0" user_nickname="익명" user_picture="https://cf-fpi.everytime.kr/0.png"/>
#   <article id="56939979" is_mine="0" title="" text="박진서 통입 vs 박혜준 통입&lt;br />박혜준 교수님에 관한 이야기는 심지어 에타에 없는데&lt;br />계절에 어떤 분꺼 듣는게 나을까요??" created_at="2018-11-15 11:05:22" posvote="0" comment="0" comment_anonym="0" scrap_count="0" user_type="" user_id="0" user_nickname="익명" user_picture="https://cf-fpi.everytime.kr/0.png"/>
#   <article id="56939954" is_mine="0" title="" text="점심 해장라면 vs 부대라면&lt;br />&lt;br />뭐머글까??" created_at="2018-11-15 11:04:57" posvote="0" comment="1" comment_anonym="0" scrap_count="0" user_type="" user_id="0" user_nickname="익명" user_picture="https://cf-fpi.everytime.kr/0.png"/>
#   <article id="56939875" is_mine="0" title="" text="교육관 여휴없냐 ㅠ" created_at="2018-11-15 11:03:35" posvote="0" comment="0" comment_anonym="0" scrap_count="0" user_type="" user_id="0" user_nickname="익명" user_picture="https://cf-fpi.everytime.kr/0.png"/>
#   <article id="56939342" is_mine="0" title="" text="오늘 학교오면서 아파트 엘베에서 수능보러가는 고3을봤어요&lt;br />&lt;br />가장 친숙한 복장인 교복입고 부모님 손잡고 같이가던데&lt;br />&lt;br />제 옛날생각이 나더라구요&lt;br />&lt;br />괜히 나까지 갬성...." created_at="2018-11-15 10:55:15" posvote="1" comment="2" comment_anonym="0" scrap_count="0" user_type="" user_id="0" user_nickname="익명" user_picture="https://cf-fpi.everytime.kr/0.png"/>
#   <article id="56939139" is_mine="0" title="" text="신중도에서 빌린 책 중도 무인데스크에서 반납 가능?" created_at="2018-11-15 10:51:56" posvote="0" comment="1" comment_anonym="0" scrap_count="0" user_type="" user_id="0" user_nickname="익명" user_picture="https://cf-fpi.everytime.kr/0.png"/>
#   <article id="56938978" is_mine="0" title="" text="여자들 남친이 시간 갖자그러면 무슨 기분이야??" created_at="2018-11-15 10:49:18" posvote="0" comment="4" comment_anonym="0" scrap_count="0" user_type="" user_id="0" user_nickname="익명" user_picture="https://cf-fpi.everytime.kr/0.png"/>
#   <article id="56938619" is_mine="0" title="" text="지금 전공 좋아지고 잘하고싶다&lt;br />중간 잘보긴했는데 좀 아쉬워&lt;br />기말엔 진짜 에이쁠상위권처럼&lt;br />완전히 통달하는거시 목표다!!! 대학원가즈아" created_at="2018-11-15 10:43:25" posvote="0" comment="0" comment_anonym="0" scrap_count="0" user_type="" user_id="0" user_nickname="익명" user_picture="https://cf-fpi.everytime.kr/0.png"/>
#   <article id="56937514" is_mine="0" title="" text="어제 헤어졌는데 사람 마음이 그렇게 한순간에 변할 수 있다는게 어벙벙해서 자꾸 다시 연락하고 싶어짐" created_at="2018-11-15 10:24:00" posvote="0" comment="1" comment_anonym="0" scrap_count="0" user_type="" user_id="0" user_nickname="익명" user_picture="https://cf-fpi.everytime.kr/0.png"/>
#   <article id="56937094" is_mine="0" title="" text="너네 지금 먹고싶은거 뭐임" created_at="2018-11-15 10:15:45" posvote="0" comment="5" comment_anonym="0" scrap_count="0" user_type="" user_id="0" user_nickname="익명" user_picture="https://cf-fpi.everytime.kr/0.png"/>
#   <article id="56937029" is_mine="0" title="" text="틴더 나한테 좋아요한 사람 누구인지 보려면 돈 내야하는거야...?" created_at="2018-11-15 10:14:31" posvote="0" comment="1" comment_anonym="0" scrap_count="0" user_type="" user_id="0" user_nickname="익명" user_picture="https://cf-fpi.everytime.kr/0.png"/>
#   <article id="56936755" is_mine="0" title="" text="전과 내년에 신청하려는 곧 헌내기 되는 새내기인데&lt;br />자소서 마니 중요한가요...? 뭐 물어보나요...?" created_at="2018-11-15 10:09:16" posvote="0" comment="0" comment_anonym="0" scrap_count="0" user_type="" user_id="0" user_nickname="익명" user_picture="https://cf-fpi.everytime.kr/0.png"/>
#   <article id="56936652" is_mine="0" title="" text="오늘 신우진 교수님 미원 녹강 구합니다 서례 드립니다!!" created_at="2018-11-15 10:06:51" posvote="0" comment="1" comment_anonym="0" scrap_count="0" user_type="" user_id="0" user_nickname="익명" user_picture="https://cf-fpi.everytime.kr/0.png"/>
#   <article id="56936243" is_mine="0" title="" text="궁금해서 틴더 깔아봤는데 외국인 개많네 신기" created_at="2018-11-15 09:58:32" posvote="0" comment="1" comment_anonym="0" scrap_count="0" user_type="" user_id="0" user_nickname="익명" user_picture="https://cf-fpi.everytime.kr/0.png"/>
#   <article id="56936042" is_mine="0" title="" text="교직인적성검사는 뭘 물어보는거에요..??" created_at="2018-11-15 09:54:08" posvote="0" comment="0" comment_anonym="0" scrap_count="0" user_type="" user_id="0" user_nickname="익명" user_picture="https://cf-fpi.everytime.kr/0.png"/>
#   <article id="56935416" is_mine="0" title="" text="다 같이 연고전가서 연대 응원하고&lt;br />아카라카가서 연뽕 주입한 줄 알았는데&lt;br />속으로 다 튈 각 재고 있었구나&lt;br />이렇게 많은 줄은 몰랐어&lt;br />내가 아는 이과생 몇 안되지만 그 중 절반 이상이야..." created_at="2018-11-15 09:40:37" posvote="0" comment="1" comment_anonym="0" scrap_count="0" user_type="" user_id="0" user_nickname="익명" user_picture="https://cf-fpi.everytime.kr/0.png"/>
#   <article id="56935033" is_mine="0" title="" text="학교게시판 좋아&lt;br />가끔 말 안통하고 욕박는 이상한 애들도 있지만&lt;br />그 비율이 현저히 적은거 같앙&lt;br />힘들 때 와야지" created_at="2018-11-15 09:31:15" posvote="0" comment="2" comment_anonym="0" scrap_count="0" user_type="" user_id="0" user_nickname="익명" user_picture="https://cf-fpi.everytime.kr/0.png"/>
#   <article id="56934798" is_mine="0" title="" text="여친이 타대생인데 졸업시험 토요일에 있다고 해서 바쁠테니 편할때 연락하라했어. 근데 프사는 바뀌어있고 힘내라는 카톡은 읽씹하네. 바빠서 그런걸까 아니면 뭐 다른게 있는걸까." created_at="2018-11-15 09:25:00" posvote="0" comment="9" comment_anonym="0" scrap_count="0" user_type="" user_id="0" user_nickname="익명" user_picture="https://cf-fpi.everytime.kr/0.png"/>
# </response>

# parse_post url example
# <?xml version="1.0" encoding="UTF-8"?>
# <response>
#   <article id="56884342" is_mine="0" title="학교사무실 실화냐" text="아니 전화를 아무리해도 안받아서 직접찾아가보니&lt;br />&lt;br />전화왔을때 메시지나오는걸로 돌려놓고&lt;br />&lt;br />쉬고있더라&lt;br />&lt;br />가서 원하는 업무 말하니 처리하고&lt;br />&lt;br />혹시나해서 나오면서 전화걸었는데 울리지도않고&lt;br />&lt;br />아까랑같은 메시지 뜨더라 (다른전화응대로 통화어렵다)&lt;br />&lt;br />이거 미친거아니냐진짜 장학팀도그러더니&lt;br />&lt;br />이거 핫게가야됨진짜...&lt;br />&lt;br />학교교직원 진짜 너무하네" created_at="2018-11-14 17:28:21" posvote="240" comment="25" comment_anonym="0" scrap_count="5" hasNotification="0" user_type="" user_id="0" user_nickname="익명" user_picture="https://cf-fpi.everytime.kr/0.png"/>
#   <comment id="256912540" parent_id="0" is_mine="0" text="이거 아까 똑같은 글 있지 않았음?" created_at="2018-11-14 17:30:47" hasNotification="0" user_type="" user_id="0" user_nickname="익명" user_picture="https://cf-fpi.everytime.kr/0.png"/>
#   <comment id="256933623" parent_id="0" is_mine="0" text="이거 아까 공대라고 떴던거 아니냐? 근데 내가 가본 공대 사무실에선 한번도 그랬던적이 없었는데. 잘못된 번호로 전화했던게 아니냐?" created_at="2018-11-14 18:45:56" hasNotification="0" user_type="" user_id="0" user_nickname="익명" user_picture="https://cf-fpi.everytime.kr/0.png"/>
#   <comment id="257066224" parent_id="0" is_mine="0" text="학교 어디사무실?" created_at="2018-11-14 23:37:34" hasNotification="0" user_type="" user_id="0" user_nickname="익명" user_picture="https://cf-fpi.everytime.kr/0.png"/>
#   <comment id="257072832" parent_id="0" is_mine="0" text="저번학기부터 임금가지고 ㅈㄹ하잖아 그거땜에 일부러 저러는거" created_at="2018-11-14 23:48:01" hasNotification="0" user_type="" user_id="0" user_nickname="익명" user_picture="https://cf-fpi.everytime.kr/0.png"/>
#   <comment id="257212918" parent_id="257072832" is_mine="0" text="파업이라는거임?" created_at="2018-11-15 10:30:20" user_type="" user_id="0" user_nickname="익명" user_picture="https://cf-fpi.everytime.kr/0.png"/>
#   <comment id="257076038" parent_id="0" is_mine="0" text="ㅋㅋㅋㅋㅋㅋㅋㅋㅋ학부생 등록금 꺼-억 맛있노 이기야" created_at="2018-11-14 23:53:06" hasNotification="0" user_type="" user_id="0" user_nickname="익명" user_picture="https://cf-fpi.everytime.kr/0.png"/>
#   <comment id="257212725" parent_id="0" is_mine="0" text="몇시에 전화했는데" created_at="2018-11-15 10:29:39" hasNotification="0" user_type="" user_id="0" user_nickname="익명" user_picture="https://cf-fpi.everytime.kr/0.png"/>
#   <comment id="257212792" parent_id="0" is_mine="0" text="우리 공대 학부 사무실은 안그런데..." created_at="2018-11-15 10:29:56" hasNotification="0" user_type="" user_id="0" user_nickname="익명" user_picture="https://cf-fpi.everytime.kr/0.png"/>
#   <comment id="257213627" parent_id="0" is_mine="0" text="사실일 걸. 나도 본 적 있다." created_at="2018-11-15 10:33:00" hasNotification="0" user_type="" user_id="0" user_nickname="익명" user_picture="https://cf-fpi.everytime.kr/0.png"/>
#   <comment id="257214694" parent_id="0" is_mine="0" text="공대는 아님 개인적으론" created_at="2018-11-15 10:36:42" hasNotification="0" user_type="" user_id="0" user_nickname="익명" user_picture="https://cf-fpi.everytime.kr/0.png"/>
#   <comment id="257217097" parent_id="0" is_mine="0" text="사람새끼들이냐 ㅋㅋ" created_at="2018-11-15 10:44:34" hasNotification="0" user_type="" user_id="0" user_nickname="익명" user_picture="https://cf-fpi.everytime.kr/0.png"/>
#   <comment id="257217858" parent_id="0" is_mine="0" text="어디 사무실의 직원인지가 중요할듯 공식적으로 항의하려면" created_at="2018-11-15 10:47:01" hasNotification="0" user_type="" user_id="0" user_nickname="익명" user_picture="https://cf-fpi.everytime.kr/0.png"/>
#   <comment id="257218810" parent_id="0" is_mine="0" text="옛날부터 4시 넘어가면 전화기 끄고 퇴근 미리 준비한단 소문 있었음ㅎㅎ" created_at="2018-11-15 10:50:15" hasNotification="0" user_type="" user_id="0" user_nickname="익명" user_picture="https://cf-fpi.everytime.kr/0.png"/>
#   <comment id="257220005" parent_id="0" is_mine="0" text="중요한건 매우 많이 배우신 초고스펙분들이라능거.." created_at="2018-11-15 10:54:02" hasNotification="0" user_type="" user_id="0" user_nickname="익명" user_picture="https://cf-fpi.everytime.kr/0.png"/>
#   <comment id="257220028" parent_id="0" is_mine="0" text="어디사무실인데?" created_at="2018-11-15 10:54:06" hasNotification="0" user_type="" user_id="0" user_nickname="익명" user_picture="https://cf-fpi.everytime.kr/0.png"/>
#   <comment id="257228226" parent_id="0" is_mine="0" text="진짜 뭐를 위한 교직원인지 모르겠음 학교일 학생일 처리하라고 있는곳인데 대학 생활 내내 도움 받은 적 한 번 도 없다고 단언하고, 심지어 도움 받기 위해 연락 했으나 본인들이 못하는 거라며(본인들이 못하면 누가하지?) 일처리 능력은 물론 학생들 상대하는 예의라는 개념이 없는 말투나 서로 다른 단과대로 미루는 행동까지 진심으로 배운 사람들 맞나 싶은 모습임.. 비판받아 마땅하고 고쳐져야 마땅함. 선배의 선배때부터 계속 되던 건데 강하게 나가서 뿌리뽑아야 함." created_at="2018-11-15 11:18:18" hasNotification="0" user_type="" user_id="0" user_nickname="익명" user_picture="https://cf-fpi.everytime.kr/0.png"/>
#   <comment id="257233609" parent_id="257228226" is_mine="0" text="ㅇㅈ.." created_at="2018-11-15 11:33:13" user_type="" user_id="0" user_nickname="익명" user_picture="https://cf-fpi.everytime.kr/0.png"/>
#   <comment id="257238987" parent_id="0" is_mine="0" text="교목실도 완전 별로야 진짜... 완전 짜증나" created_at="2018-11-15 11:47:47" hasNotification="0" user_type="" user_id="0" user_nickname="익명" user_picture="https://cf-fpi.everytime.kr/0.png"/>
#   <comment id="257272822" parent_id="257238987" is_mine="0" text="교목실은 찐따들의 모임임" created_at="2018-11-15 13:12:34" user_type="" user_id="0" user_nickname="익명" user_picture="https://cf-fpi.everytime.kr/0.png"/>
#   <comment id="257280505" parent_id="257238987" is_mine="0" text="교목실은 어떤데?" created_at="2018-11-15 13:32:29" user_type="" user_id="0" user_nickname="익명" user_picture="https://cf-fpi.everytime.kr/0.png"/>
#   <comment id="257288736" parent_id="0" is_mine="0" text="ㄹㅇ 정규직의 폐해.. 적폐오브 적폐라고 본다." created_at="2018-11-15 13:55:03" hasNotification="0" user_type="" user_id="0" user_nickname="익명" user_picture="https://cf-fpi.everytime.kr/0.png"/>
#   <comment id="257288929" parent_id="0" is_mine="0" text="이분들부터 노동 유연화의 대상이 되어야 함.." created_at="2018-11-15 13:55:40" hasNotification="0" user_type="" user_id="0" user_nickname="익명" user_picture="https://cf-fpi.everytime.kr/0.png"/>
#   <comment id="257290214" parent_id="0" is_mine="0" text="근데 이사람들은 왜 법적으로 정규직 보호 받고 청소경비노동자들은 시도때도 없이 짤린다고 협박받음? 난 청소하시는 분들은 뭐 그런일 하실분이나 할수 있어하는 분들이 더 많아서라고 알고있는데 그건 교직원도 마찬가지 아님? 걍 고졸 데려다놔도 할수있는 업무인데 그 사람들아니어도 할사람 많은거.. 똑같은듯" created_at="2018-11-15 13:59:04" hasNotification="0" user_type="" user_id="0" user_nickname="익명" user_picture="https://cf-fpi.everytime.kr/0.png"/>
#   <comment id="257300919" parent_id="257290214" is_mine="0" text="시험을 보느냐 마냐의 차이인듯" created_at="2018-11-15 14:29:09" user_type="" user_id="0" user_nickname="익명" user_picture="https://cf-fpi.everytime.kr/0.png"/>
#   <comment id="257315147" parent_id="257290214" is_mine="0" text="교직원도솔직히 ㄹㅇ 빡대가리" created_at="2018-11-15 15:12:04" user_type="" user_id="0" user_nickname="익명" user_picture="https://cf-fpi.everytime.kr/0.png"/>
# </response>
