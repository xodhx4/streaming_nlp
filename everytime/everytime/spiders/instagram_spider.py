import scrapy
import json
import datetime
import os
import re
from everytime import items


class InstaSpider(scrapy.Spider):
    name = "instagram"

    custom_settings = {
        #TODO : InstagramMongoPipeline
        "MONGO_DATABASE": 'Instagram',
        'LOG_FILE': 'everytime/logs/instagram.log',
        'ITEM_PIPELINES': {
            'everytime.pipelines.InstagramMongoPipeline': 300,
        },
        "EXTENSIONS" : {
            'scrapy_kafka_export.KafkaItemExporterExtension' : 1,
        },
        "KAFKA_EXPORT_ENABLED" : True,
        "KAFKA_BROKERS" : ['kafka:9092'],
        "KAFKA_TOPIC" : 'instagram',
    }

    def __init__(self, keyword_list=None, *args, **kwargs):
        super(InstaSpider, self).__init__(*args, **kwargs)
        if keyword_list == None:
            self.keyword_list = ["20대", "연세대", "서울대", "고려대", "청춘", "대학생"]
        else:
            # TODO : read the text file of keyword
            pass
        self.p = re.compile('#[^#\s,;]+')
        self.time_now = datetime.datetime.utcnow() + datetime.timedelta(hours=9)

    def start_requests(self):
        urls = [
            f"https://www.instagram.com/explore/tags/{keyword}/?__a=1" for keyword in self.keyword_list]
        for index, url in enumerate(urls):
            yield scrapy.Request(url=url, callback=self.parse_post_url, meta={"level": 0, "hashtag" : self.keyword_list[index]})
    # https://doc.scrapy.org/en/latest/topics/request-response.html#using-formrequest-from-response-to-simulate-a-user-login

    def parse_post_url(self, response):
        self.logger.info(f"Start parse_post_url | {response.url}")
        base_url = "https://www.instagram.com/p/"
        # print(response.body)
        graphql = json.loads(response.body)["graphql"]["hashtag"]
        new_posts = graphql["edge_hashtag_to_media"]["edges"]
        popular_posts = graphql["edge_hashtag_to_top_posts"]["edges"]

        for post in new_posts:
            shortcode = post["node"]["shortcode"]
            response.meta['shortcode'] = shortcode
            url = base_url + shortcode + "/"
            yield scrapy.Request(url=url, callback=self.parse_new_post, meta=response.meta)

        for post in popular_posts:
            shortcode = post["node"]["shortcode"]
            response.meta['shortcode'] = shortcode
            url = base_url + shortcode + "/"
            yield scrapy.Request(url=url, callback=self.parse_pop_post, meta=response.meta)

    def parse_new_post(self, response):
        # TODO : reculsive tag call
        self.logger.info(f"Start parse_new_post | {response.url}")

        info = self.parse_post(response, "new")

        return info

    def parse_pop_post(self, response):
        self.logger.info(f"Start parse_pop_post | {response.url}")

        info =self.parse_post(response, "pop")

        return info


    def parse_post(self, response, typeof):

        info_json = response.xpath('//body/script').extract_first()[52:-10]
        # text = response.body
        info_json = json.loads(info_json)[
            "entry_data"]["PostPage"][0]["graphql"]["shortcode_media"]
        level = response.meta["level"]
        hashtag = response.meta["hashtag"]
        shortcode = response.meta['shortcode']

        try:
            text = info_json["edge_media_to_caption"]["edges"][0]["node"]["text"]
            comments = [[comment["node"]["text"], comment["node"]["edge_liked_by"]["count"]]
                        for comment in info_json["edge_media_to_comment"]["edges"]]
            likes = info_json["edge_media_preview_like"]["count"]
        except Exception:
            self.logger.error(info_json)
            text = ""
            comments = []
            likes = 0
        
        info = items.InstagramPostItem()
        info['hashtag'] = hashtag
        info['shortcode'] = shortcode
        info['typeof'] = typeof
        info['level'] = level
        info['text'] = text
        info['comments'] =  list()
        info['likes'] = likes
        info['crawl_time'] = self.time_now

        tags = list()
        tags += re.findall(self.p, text)

        for comment in comments:
            comment_item = items.InstagramCommentItem()
            comment_item['text'] = comment[0]
            comment_item['likes'] = comment[1]
            info['comments'].append(dict(comment_item))
            tags += re.findall(self.p, comment[0])
        info['tags'] = tags

        self.logger.info(f"{typeof}_post | {info}")
        return info
