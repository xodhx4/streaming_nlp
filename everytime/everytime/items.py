# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# https://doc.scrapy.org/en/latest/topics/items.html

import scrapy


class EverytimePostItem(scrapy.Item):
    # define the fields for your item here like:
    # name = scrapy.Field()
    moim = scrapy.Field()
    article_id = scrapy.Field()
    title = scrapy.Field()
    text = scrapy.Field()
    created_at = scrapy.Field()
    posvote = scrapy.Field()
    comment_num = scrapy.Field()
    comment = scrapy.Field()

class EverytimeCommentItem(scrapy.Item):
    comment_id = scrapy.Field()
    text = scrapy.Field()
    created_at = scrapy.Field()

class InstagramPostItem(scrapy.Item):
    hashtag = scrapy.Field()
    shortcode = scrapy.Field()
    typeof = scrapy.Field()
    level = scrapy.Field()
    text = scrapy.Field()
    comments = scrapy.Field()
    likes = scrapy.Field()
    tags = scrapy.Field()
    crawl_time = scrapy.Field()

class InstagramCommentItem(scrapy.Item):
    text = scrapy.Field()
    likes = scrapy.Field()

# parse_post url example
# <?xml version="1.0" encoding="UTF-8"?>
# <response>
#   <article id="56884342" is_mine="0" title="학교사무실 실화냐" text="아니 전화를 아무리해도 안받아서 직접찾아가보니&lt;br />&lt;br />전화왔을때 메시지나오는걸로 돌려놓고&lt;br />&lt;br />쉬고있더라&lt;br />&lt;br />가서 원하는 업무 말하니 처리하고&lt;br />&lt;br />혹시나해서 나오면서 전화걸었는데 울리지도않고&lt;br />&lt;br />아까랑같은 메시지 뜨더라 (다른전화응대로 통화어렵다)&lt;br />&lt;br />이거 미친거아니냐진짜 장학팀도그러더니&lt;br />&lt;br />이거 핫게가야됨진짜...&lt;br />&lt;br />학교교직원 진짜 너무하네" created_at="2018-11-14 17:28:21" posvote="240" comment="25" comment_anonym="0" scrap_count="5" hasNotification="0" user_type="" user_id="0" user_nickname="익명" user_picture="https://cf-fpi.everytime.kr/0.png"/>
#   <comment id="256912540" parent_id="0" is_mine="0" text="이거 아까 똑같은 글 있지 않았음?" created_at="2018-11-14 17:30:47" hasNotification="0" user_type="" user_id="0" user_nickname="익명" user_picture="https://cf-fpi.everytime.kr/0.png"/>
#   <comment id="256933623" parent_id="0" is_mine="0" text="이거 아까 공대라고 떴던거 아니냐? 근데 내가 가본 공대 사무실에선 한번도 그랬던적이 없었는데. 잘못된 번호로 전화했던게 아니냐?" created_at="2018-11-14 18:45:56" hasNotification="0" user_type="" user_id="0" user_nickname="익명" user_picture="https://cf-fpi.everytime.kr/0.png"/>
#   <comment id="257066224" parent_id="0" is_mine="0" text="학교 어디사무실?" created_at="2018-11-14 23:37:34" hasNotification="0" user_type="" user_id="0" user_nickname="익명" user_picture="https://cf-fpi.everytime.kr/0.png"/>
#   <comment id="257072832" parent_id="0" is_mine="0" text="저번학기부터 임금가지고 ㅈㄹ하잖아 그거땜에 일부러 저러는거" created_at="2018-11-14 23:48:01" hasNotification="0" user_type="" user_id="0" user_nickname="익명" user_picture="https://cf-fpi.everytime.kr/0.png"/>
#   <comment id="257212918" parent_id="257072832" is_mine="0" text="파업이라는거임?" created_at="2018-11-15 10:30:20" user_type="" user_id="0" user_nickname="익명" user_picture="https://cf-fpi.everytime.kr/0.png"/>
#   <comment id="257076038" parent_id="0" is_mine="0" text="ㅋㅋㅋㅋㅋㅋㅋㅋㅋ학부생 등록금 꺼-억 맛있노 이기야" created_at="2018-11-14 23:53:06" hasNotification="0" user_type="" user_id="0" user_nickname="익명" user_picture="https://cf-fpi.everytime.kr/0.png"/>
#   <comment id="257212725" parent_id="0" is_mine="0" text="몇시에 전화했는데" created_at="2018-11-15 10:29:39" hasNotification="0" user_type="" user_id="0" user_nickname="익명" user_picture="https://cf-fpi.everytime.kr/0.png"/>
#   <comment id="257212792" parent_id="0" is_mine="0" text="우리 공대 학부 사무실은 안그런데..." created_at="2018-11-15 10:29:56" hasNotification="0" user_type="" user_id="0" user_nickname="익명" user_picture="https://cf-fpi.everytime.kr/0.png"/>
#   <comment id="257213627" parent_id="0" is_mine="0" text="사실일 걸. 나도 본 적 있다." created_at="2018-11-15 10:33:00" hasNotification="0" user_type="" user_id="0" user_nickname="익명" user_picture="https://cf-fpi.everytime.kr/0.png"/>
#   <comment id="257214694" parent_id="0" is_mine="0" text="공대는 아님 개인적으론" created_at="2018-11-15 10:36:42" hasNotification="0" user_type="" user_id="0" user_nickname="익명" user_picture="https://cf-fpi.everytime.kr/0.png"/>
#   <comment id="257217097" parent_id="0" is_mine="0" text="사람새끼들이냐 ㅋㅋ" created_at="2018-11-15 10:44:34" hasNotification="0" user_type="" user_id="0" user_nickname="익명" user_picture="https://cf-fpi.everytime.kr/0.png"/>
#   <comment id="257217858" parent_id="0" is_mine="0" text="어디 사무실의 직원인지가 중요할듯 공식적으로 항의하려면" created_at="2018-11-15 10:47:01" hasNotification="0" user_type="" user_id="0" user_nickname="익명" user_picture="https://cf-fpi.everytime.kr/0.png"/>
#   <comment id="257218810" parent_id="0" is_mine="0" text="옛날부터 4시 넘어가면 전화기 끄고 퇴근 미리 준비한단 소문 있었음ㅎㅎ" created_at="2018-11-15 10:50:15" hasNotification="0" user_type="" user_id="0" user_nickname="익명" user_picture="https://cf-fpi.everytime.kr/0.png"/>
#   <comment id="257220005" parent_id="0" is_mine="0" text="중요한건 매우 많이 배우신 초고스펙분들이라능거.." created_at="2018-11-15 10:54:02" hasNotification="0" user_type="" user_id="0" user_nickname="익명" user_picture="https://cf-fpi.everytime.kr/0.png"/>
#   <comment id="257220028" parent_id="0" is_mine="0" text="어디사무실인데?" created_at="2018-11-15 10:54:06" hasNotification="0" user_type="" user_id="0" user_nickname="익명" user_picture="https://cf-fpi.everytime.kr/0.png"/>
#   <comment id="257228226" parent_id="0" is_mine="0" text="진짜 뭐를 위한 교직원인지 모르겠음 학교일 학생일 처리하라고 있는곳인데 대학 생활 내내 도움 받은 적 한 번 도 없다고 단언하고, 심지어 도움 받기 위해 연락 했으나 본인들이 못하는 거라며(본인들이 못하면 누가하지?) 일처리 능력은 물론 학생들 상대하는 예의라는 개념이 없는 말투나 서로 다른 단과대로 미루는 행동까지 진심으로 배운 사람들 맞나 싶은 모습임.. 비판받아 마땅하고 고쳐져야 마땅함. 선배의 선배때부터 계속 되던 건데 강하게 나가서 뿌리뽑아야 함." created_at="2018-11-15 11:18:18" hasNotification="0" user_type="" user_id="0" user_nickname="익명" user_picture="https://cf-fpi.everytime.kr/0.png"/>
#   <comment id="257233609" parent_id="257228226" is_mine="0" text="ㅇㅈ.." created_at="2018-11-15 11:33:13" user_type="" user_id="0" user_nickname="익명" user_picture="https://cf-fpi.everytime.kr/0.png"/>
#   <comment id="257238987" parent_id="0" is_mine="0" text="교목실도 완전 별로야 진짜... 완전 짜증나" created_at="2018-11-15 11:47:47" hasNotification="0" user_type="" user_id="0" user_nickname="익명" user_picture="https://cf-fpi.everytime.kr/0.png"/>
#   <comment id="257272822" parent_id="257238987" is_mine="0" text="교목실은 찐따들의 모임임" created_at="2018-11-15 13:12:34" user_type="" user_id="0" user_nickname="익명" user_picture="https://cf-fpi.everytime.kr/0.png"/>
#   <comment id="257280505" parent_id="257238987" is_mine="0" text="교목실은 어떤데?" created_at="2018-11-15 13:32:29" user_type="" user_id="0" user_nickname="익명" user_picture="https://cf-fpi.everytime.kr/0.png"/>
#   <comment id="257288736" parent_id="0" is_mine="0" text="ㄹㅇ 정규직의 폐해.. 적폐오브 적폐라고 본다." created_at="2018-11-15 13:55:03" hasNotification="0" user_type="" user_id="0" user_nickname="익명" user_picture="https://cf-fpi.everytime.kr/0.png"/>
#   <comment id="257288929" parent_id="0" is_mine="0" text="이분들부터 노동 유연화의 대상이 되어야 함.." created_at="2018-11-15 13:55:40" hasNotification="0" user_type="" user_id="0" user_nickname="익명" user_picture="https://cf-fpi.everytime.kr/0.png"/>
#   <comment id="257290214" parent_id="0" is_mine="0" text="근데 이사람들은 왜 법적으로 정규직 보호 받고 청소경비노동자들은 시도때도 없이 짤린다고 협박받음? 난 청소하시는 분들은 뭐 그런일 하실분이나 할수 있어하는 분들이 더 많아서라고 알고있는데 그건 교직원도 마찬가지 아님? 걍 고졸 데려다놔도 할수있는 업무인데 그 사람들아니어도 할사람 많은거.. 똑같은듯" created_at="2018-11-15 13:59:04" hasNotification="0" user_type="" user_id="0" user_nickname="익명" user_picture="https://cf-fpi.everytime.kr/0.png"/>
#   <comment id="257300919" parent_id="257290214" is_mine="0" text="시험을 보느냐 마냐의 차이인듯" created_at="2018-11-15 14:29:09" user_type="" user_id="0" user_nickname="익명" user_picture="https://cf-fpi.everytime.kr/0.png"/>
#   <comment id="257315147" parent_id="257290214" is_mine="0" text="교직원도솔직히 ㄹㅇ 빡대가리" created_at="2018-11-15 15:12:04" user_type="" user_id="0" user_nickname="익명" user_picture="https://cf-fpi.everytime.kr/0.png"/>
# </response>