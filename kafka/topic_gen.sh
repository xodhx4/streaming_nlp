#!/bin/bash
everytime_topic_num=$1
instagram_topic_num=$2

echo everytime_topic_num is $everytime_topic_num
echo instagram_topic_num is $instagram_topic_num

sh $KAFKA_HOME/bin/kafka-topics.sh --create \
    --zookeeper localhost:2181 \
    --topic everytime \
    --partitions $everytime_topic_num \
    --replication-factor 1

sh $KAFKA_HOME/bin/kafka-topics.sh --create \
    --zookeeper localhost:2181 \
    --topic instagram \
    --partitions $instagram_topic_num \
    --replication-factor 1
